# Loop Player

Loop audio files with loop start and end points.

## Building

`make`

### Dependencies
- `libsamplerate0-dev`
- `libsndfile1-dev`
- `libjack-jackd2-dev` or `libjack-dev`

## Usage

To prepare an audio file, simply add `loop_start:#,loop_end:#` with the sample numbers of the loop start and end points to the comment metadata tag of your audio file.

Consult `loop-player --help` for usage. You can also manually supply the loop points with arguments.
