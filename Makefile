CC=gcc
FLAGS=`pkg-config --cflags sndfile samplerate jack`
LIBS=`pkg-config --libs sndfile samplerate jack`
PREFIX=/usr
BUILD_DIR=build
SRCS = $(shell find ./src/*.c)
OBJS = $(patsubst %.c, $(BUILD_DIR)/%.o, $(SRCS))
OPTIMIZATION=-O3
EXECUTABLE=loop-player

all: $(BUILD_DIR)/$(EXECUTABLE)

$(BUILD_DIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c -o $@ $(FLAGS) $< -Wall $(OPTIMIZATION)

$(BUILD_DIR)/$(EXECUTABLE): $(OBJS)
	$(CC) -o $@ $^ $(LIBS) $(OPTIMIZATION)

clean:
	rm -rf build
