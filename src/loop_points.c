#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void get_loop_points(const char *str, long *loop_start, long *loop_end){
    #define START_REGEX "loop_start:([0-9]+)"
    #define END_REGEX "loop_end:([0-9]+)"

    regex_t start_regex;
    regcomp(&start_regex, START_REGEX, REG_ICASE | REG_EXTENDED);
    regex_t end_regex;
    regcomp(&end_regex, END_REGEX, REG_ICASE | REG_EXTENDED);

    regmatch_t start_matches[2];
    int start_match = regexec(&start_regex, str, 2, start_matches, 0);
    if(start_match == 0){
        int match_chars = start_matches[1].rm_eo - start_matches[1].rm_so;
        if(match_chars <= 10){ // Crazy numbers that don't fit in a long
            char matched_str[match_chars + 1];
            matched_str[match_chars] = '\0';
            memcpy(matched_str, str + start_matches[1].rm_so, match_chars);
            *loop_start = strtol(matched_str, NULL, 10);
        }
    }

    regmatch_t end_matches[2];
    int end_match = regexec(&end_regex, str, 2, end_matches, 0);
    if(end_match == 0){
        int match_chars = end_matches[1].rm_eo - end_matches[1].rm_so;
        if(match_chars <= 10){ // Crazy numbers that don't fit in a long
            char matched_str[match_chars + 1];
            matched_str[match_chars] = '\0';
            memcpy(matched_str, str + end_matches[1].rm_so, match_chars);
            *loop_end = strtol(matched_str, NULL, 10);
        }
    }

    regfree(&start_regex);
    regfree(&end_regex);
}
