#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

struct termios stdin_tty;
tcflag_t orig_flags;

static void clear_stdin(){
    int chars;
    ioctl(STDIN_FILENO, FIONREAD, &chars);

    for(int i = 0; i < chars; i++){
        char readbyte;
        read(STDIN_FILENO, &readbyte, 1);
    }
}

void start_controls(){
    tcgetattr(STDIN_FILENO, &stdin_tty);
    orig_flags = stdin_tty.c_lflag;
    stdin_tty.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &stdin_tty);

    clear_stdin();
}

char read_stdin(){
    int chars;
    ioctl(STDIN_FILENO, FIONREAD, &chars);
    if(chars > 0){
        char readbyte;
        read(STDIN_FILENO, &readbyte, 1);
        return readbyte;
    }
    return '\0';
}

void stop_controls(){
    clear_stdin();

    stdin_tty.c_lflag = orig_flags;
    tcsetattr(STDIN_FILENO, TCSANOW, &stdin_tty);
}