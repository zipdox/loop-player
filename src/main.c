// loop-player
// Copyright (C) 2023  Zipdox

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdio.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <argp.h>
#include <sndfile.h>
#include <jack/jack.h>
#include <samplerate.h>

#include "controls.h"
#include "loop_points.h"

const char *argp_program_version = "loop-player 0.1";
const char *argp_program_bug_address = "<loop-player@zipdox.net>";

struct argp_option options[] = {
    {"loop_start", 's', "START", 0, "Override for loop start frame"},
    {"loop_end", 'e', "END", 0, "Override for loop end frame"},
    {"device", 'd', "REGEX", 0, "RegEx for ports to connect to"},
    {"volume", 'v', "VOLUME", 0, "Playback volume"},
    {0}
};

long loop_start_arg = -1;
long loop_end_arg = -1;
char *port_regex = "system:.+";
char *filename = NULL;
float volume = 1.0f;

error_t parse_options(int key, char *arg, struct argp_state *state){
    switch(key){
        case 's':
            loop_start_arg = strtol(arg, NULL, 10);
            break;
        case 'e':
            loop_end_arg = strtol(arg, NULL, 10);
            break;
        case 'd':
            port_regex = arg;
            break;
        case 'v':
            volume = strtof(arg, NULL);
            break;
        case ARGP_KEY_ARG:
            filename = arg;
            break;
        case ARGP_KEY_NO_ARGS:
        case ARGP_KEY_END:
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

struct argp argp = {options, parse_options, "[FILE]", "Loops audio files with loop start and end points"};

#define JACK_CLIENT_NAME "Loop Player"

void timestamp(sf_count_t frames, int samplerate, int *minutes, int *seconds, int *csec){
    *minutes = frames / samplerate / 60;
    *seconds = frames / samplerate - 60 * (*minutes);
    *csec = frames / (samplerate / 100) - frames / samplerate * 100;
}

typedef struct {
    int ready;
    float *buffer;
    long frames;
    long loop_start, loop_end;
    long position;
    int channels;
    int finish;
    jack_port_t **ports;
    SRC_STATE *resampler;
    SRC_DATA resampler_data;
} CallbackInfo;

int process(jack_nframes_t nframes, void *arg){
    CallbackInfo *cbinfo = arg;
    if(!cbinfo->ready) return 0;
    float *bufs[cbinfo->channels];
    for(int c = 0; c < cbinfo->channels; c++){
        bufs[c] = jack_port_get_buffer(cbinfo->ports[c], nframes);
    }

    long position = cbinfo->position;
    for(jack_nframes_t i = 0; i < nframes; i++){
        if(position >= cbinfo->frames){
            for(int c = 0; c < cbinfo->channels; c++){
                bufs[c][i] = 0.0f;
            }
        }else{
            for(int c = 0; c < cbinfo->channels; c++){
                bufs[c][i] = cbinfo->buffer[cbinfo->channels * position + c] * volume;
            }
            position++;
            if(position >= cbinfo->loop_end && !cbinfo->finish) position = cbinfo->loop_start;
        }
    }
    cbinfo->position = position;

    return 0;
}

int process_resample(jack_nframes_t nframes, void *arg){
    CallbackInfo *cbinfo = arg;
    if(!cbinfo->ready) return 0;
    float *bufs[cbinfo->channels];
    for(int c = 0; c < cbinfo->channels; c++){
        bufs[c] = jack_port_get_buffer(cbinfo->ports[c], nframes);
    }
    
    double get_frames_d = (double) nframes / cbinfo->resampler_data.src_ratio * 1.5;
    jack_nframes_t get_frames = get_frames_d;

    float read_frames[get_frames * cbinfo->channels];
    
    long position = cbinfo->position;
    for(jack_nframes_t i = 0; i < get_frames; i++){
        if(position >= cbinfo->frames){
            for(int c = 0; c < cbinfo->channels; c++){
                read_frames[i * cbinfo->channels + c] = 0.0f;
            }
        }else{
            for(int c = 0; c < cbinfo->channels; c++){
                read_frames[i * cbinfo->channels + c] = cbinfo->buffer[cbinfo->channels * position + c] * volume;
            }
            position++;
            if(position >= cbinfo->loop_end && !cbinfo->finish) position = cbinfo->loop_start;
        }
    }

    float resampled_frames[nframes * cbinfo->channels];
    cbinfo->resampler_data.input_frames = get_frames;
    cbinfo->resampler_data.output_frames = nframes;
    cbinfo->resampler_data.data_in = read_frames;
    cbinfo->resampler_data.data_out = resampled_frames;
    int src_error = src_process(cbinfo->resampler, &cbinfo->resampler_data);
    if(src_error != 0)
        fprintf(stderr, "Resampling error: %s\n", src_strerror(src_error));
    
    for(jack_nframes_t i = 0; i < nframes; i++){
        for(int c = 0; c < cbinfo->channels; c++){
            bufs[c][i] = resampled_frames[i * cbinfo->channels + c];
        }
    }

    long real_position = cbinfo->position;
    for(long i = 0; i < cbinfo->resampler_data.input_frames_used; i++){
        if(real_position >= cbinfo->frames){
            cbinfo->resampler_data.end_of_input = 1;
            break;
        }else{
            real_position++;
            if(real_position >= cbinfo->loop_end && !cbinfo->finish) real_position = cbinfo->loop_start;
        }
    }
    cbinfo->position = real_position;

    return 0;
}

int main(int argc, char *argv[]){
    argp_parse(&argp, argc, argv, 0, 0, NULL);

    if(filename == NULL){
        fprintf(stderr, "No file specified\n");
        return 1;
    }

    SF_INFO file_info = {0};
    SNDFILE *file = sf_open(filename, SFM_READ, &file_info);
    if(file == NULL){
        fprintf(stderr, "%s\n", sf_strerror(NULL));
        return 1;
    }

    const char *title = sf_get_string(file, SF_STR_TITLE);
    printf("Title:       %s\n", title != NULL ? title : basename(argv[1]));
    int samplerate = file_info.samplerate;
    printf("Sample rate: %d kHz\n", samplerate);
    int channels = file_info.channels;
    printf("Channels:    %d\n", channels);
    int minutes, seconds, csec;
    sf_count_t frames = file_info.frames;
    timestamp(frames, samplerate, &minutes, &seconds, &csec);
    printf("Length:      %d:%02d.%02d (%ld frames)\n", minutes, seconds, csec, frames);

    long loop_start = -1, loop_end = -1;
    const char *comment = sf_get_string(file, SF_STR_COMMENT);
    if(comment != NULL){
        get_loop_points(comment, &loop_start, &loop_end);
    }
    if(loop_start_arg >= 0) loop_start = loop_start_arg;
    if(loop_end_arg >= 0) loop_end = loop_end_arg;
    if(loop_start < 0 || loop_end < 0){
        fprintf(stderr, "No valid loop points in file or supplied\n");
        sf_close(file);
        return 1;
    }
    timestamp(loop_start, samplerate, &minutes, &seconds, &csec);
    printf("Loop start:  %d:%02d.%02d (frame %ld)\n", minutes, seconds, csec, loop_start);
    timestamp(loop_end, samplerate, &minutes, &seconds, &csec);
    printf("Loop end:    %d:%02d.%02d (frame %ld)\n", minutes, seconds, csec, loop_end);
    if(loop_end > frames){
        fprintf(stderr, "Loop end later than last frame\n");
        sf_close(file);
        return 1;
    }
    if(loop_end <= loop_start){
        fprintf(stderr, "Loop end before loop start\n");
        sf_close(file);
        return 1;
    }

    float *buffer = malloc(frames * sizeof(float) * channels);
    if(buffer == NULL){
        fprintf(stderr, "Unable to allocate audio buffer\n");
        sf_close(file);
        return 1;
    }

    CallbackInfo cbinfo = {
        0,
        buffer,
        frames,
        loop_start, loop_end,
        0,
        channels,
        0,
        NULL,
        NULL
    };

    sf_count_t frames_read = sf_readf_float(file, buffer, frames);
    if(frames_read != frames){
        fprintf(stderr, "Could not read entire file\n");
        free(buffer);
        sf_close(file);
        return 1;
    }

    sf_close(file);

    jack_status_t status;
    jack_client_t *audio = jack_client_open(JACK_CLIENT_NAME, JackNullOption, &status, NULL);
    if(audio == NULL){
        fprintf(stderr, "Failed to open JACK client, 0x%2.0x\n", status);
        free(buffer);
        return 1;
    }

    jack_port_t *ports[channels];
    for(int i = 0; i < channels; i++){
        char port_name[16];
        sprintf(port_name, "out_%d", i);
        ports[i] = jack_port_register(audio, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    }
    cbinfo.ports = ports;

    jack_nframes_t jack_rate = jack_get_sample_rate(audio);

    if(jack_rate == samplerate){
        jack_set_process_callback(audio, process, &cbinfo);
    }else{
        int src_err;
        cbinfo.resampler = src_new(SRC_SINC_MEDIUM_QUALITY, channels, &src_err);
        if(cbinfo.resampler == NULL){
            fprintf(stderr, "Failed to create resampler: %d\n", src_err);
            jack_client_close(audio);
            free(buffer);
            return 1;
        }
        cbinfo.resampler_data.src_ratio = (double)jack_rate / (double)samplerate;
        cbinfo.resampler_data.end_of_input = 0;
        jack_set_process_callback(audio, process_resample, &cbinfo);
    }

    jack_activate(audio);

    const char **out_ports = jack_get_ports(audio, port_regex, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput);
    int connected_ports = 0;
    for(int i = 0; connected_ports < channels; i++){
        const char *out_port = out_ports[i];
        if(out_port == NULL) break;
        jack_connect(audio, jack_port_name(ports[connected_ports]), out_port);
        connected_ports++;
    }
    jack_free(out_ports);

    cbinfo.ready = 1;

    start_controls();

    printf("Press return to exit loop.\n");
    int run = 1;
    while(run){
        while(1){
            char readbyte = read_stdin();
            if(readbyte == '\n'){
                if(!cbinfo.finish){
                    cbinfo.finish = 1;
                    printf("\rExiting loop, press return again to quit.\n");
                }else{
                    run = 0;
                    break;
                }
            }else{
                break;
            }
        }
        if(cbinfo.position >= cbinfo.frames) break;
        sf_count_t pos = cbinfo.position;
        timestamp(pos, samplerate, &minutes, &seconds, &csec);
        printf("\r%d:%02d.%02d", minutes, seconds, csec);
        usleep(100);
    }

    printf("\n");

    stop_controls();

    jack_deactivate(audio);
    jack_client_close(audio);

    if(cbinfo.resampler != NULL) src_delete(cbinfo.resampler);

    free(buffer);

    return 0;
}
